Name:          peony
Version:       4.0.0.0
Release:       1
Summary:       file Manager for the UKUI desktop
License:       GPL-3.0-or-later and Expat LGPL-3+
URL:           http://www.ukui.org
Source0:       %{name}-%{version}.tar.gz
Patch01:       0001-fix-compilation-errors.patch

BuildRequires: libcanberra-devel
BuildRequires: dconf-devel
BuildRequires: glib2-devel 
BuildRequires: gsettings-qt-devel
BuildRequires: kf5-kwayland-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: libnotify-devel
BuildRequires: poppler-devel
BuildRequires: poppler-qt5-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: openssl-devel 
BuildRequires: libudisks2-devel
BuildRequires: wayland-devel
BuildRequires: libX11-devel
BuildRequires: pkg-config
BuildRequires: qt5-qtbase-devel
BuildRequires: qtchooser
BuildRequires: qt5-qtsvg-devel
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: bamf-devel
BuildRequires: libXrandr-devel
BuildRequires: libXtst-devel
BuildRequires: ukui-interface
BuildRequires: libkysdk-qtwidgets-devel
BuildRequires: libkysdk-waylandhelper-devel
BuildRequires: libkysdk-sysinfo-devel
BuildRequires: libkscreen-qt5-devel
BuildRequires: qt5-linguist

Requires: peony-common, libpeony3

%description
 Peony is the official file manager for the UKUI desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the UKUI
 desktop. It works on local and remote filesystems.

%package common
Summary:     file manager for the UKUI desktop (common files)
License:     GPL-3.0-or-later and MIT and BSD-3-Clause

%description common
 Peony is the official file manager for the UKUI desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the UKUI
 desktop. It works on local and remote filesystems.
 .
 This package contains the architecture independent files.

%package -n libpeony3
Summary:     libraries for Peony components
License:     LGPL-3.0-or-later and MIT and BSD-3-Clause
Requires:    dvd+rw-tools
Requires:    gvfs
Requires:    gvfs-afc
Requires:    libblockdev-devel
Provides:    libpeony

%description -n libpeony3
 Peony is the official file manager for the UKUI desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the UKUI
 desktop. It works on local and remote filesystems.
 .
 This package contains a few runtime libraries needed by Peony's
 extensions

%package -n libpeony-devel
Summary:     libraries for Peony components (development files)
License:     LGPL-3.0-or-later and MIT and BSD-3-Clause

%description -n libpeony-devel
 Peony is the official file manager for the UKUI desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the UKUI
 desktop. It works on local and remote filesystems.
 .
 This package contains the development files for the libraries needed
 by Peony's extensions.


%prep
%autosetup -n %{name}-%{version} -p1

%build
%{qmake_qt5}
%{make_build}

%install
rm -rf $RPM_BUILD_ROOT
%{make_install} INSTALL_ROOT=%{buildroot}

#peony-common
mkdir -p %{buildroot}/usr/share/dbus-1/interfaces
mkdir -p %{buildroot}/usr/share/dbus-1/services
mkdir -p %{buildroot}/usr/share/kylin-user-guide/data/guide/system-file-manager/

cp -r %{_builddir}/%{name}-%{version}/peony-qt-desktop/freedesktop-dbus-interfaces.xml %{buildroot}/usr/share/dbus-1/interfaces
cp -r %{_builddir}/%{name}-%{version}/peony-qt-desktop/org.ukui.freedesktop.FileManager1.service %{buildroot}/usr/share/dbus-1/services
cp -r data/system-file-manager/* %{buildroot}/usr/share/kylin-user-guide/data/guide/system-file-manager/

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%{_prefix}/bin/*
%{_datadir}/applications/*

%files common
%doc debian/copyright debian/changelog
/etc/xdg/autostart/peony-desktop.desktop
%{_datadir}/kylin-user-guide/data/guide/system-file-manager/*
%{_datadir}/glib-2.0/schemas/org.ukui.peony.settings.gschema.xml
%{_datadir}/peony-qt-desktop/*
%{_datadir}/dbus-1/interfaces/freedesktop-dbus-interfaces.xml
%{_datadir}/dbus-1/services/org.ukui.freedesktop.FileManager1.service
%{_datadir}/peony-qt/*
%{_datadir}/peony-qt-desktop/*
%{_datadir}/peony-qt/*

%files -n libpeony3
%{_prefix}/%{_lib}/*.so.*
%{_datadir}/libpeony-qt/*

%files -n libpeony-devel
%{_prefix}/include/peony-qt/*
%{_prefix}/%{_lib}/pkgconfig/*.pc
%{_prefix}/%{_lib}/*.so

%changelog
* Thu Mar 28 2024 huayadong <huayadong@kylinos.cn> - 4.0.0.0-1
- update version to 4.0.0.0

* Tue Jun 06 2023 peijiankang <peijiankang@kylinos.cn> - 3.10.0-3
- fix share error of peony

* Tue Feb 07 2023 peijiankang <peijiankang@kylinos.cn> - 3.10.0-2
- add build debuginfo and debugsource

* Mon Nov 14 2022 tanyulong <tanyulong@kylinos.cn> - 3.10.0-1
- update version 3.10.0

* Wed Jul 13 2022 peijiankang <peijiankang@kylinos.cn> - 3.2.4-4
- add qt5-qttranslations

* Thu Jun 9 2022 peijiankang <peijiankang@kylinos.cn> - 3.2.4-3
- add kylin-user-guide files 

* Wed Apr 27 2022 wangyueliang <wangyueliang@kylinos.cn> - 3.2.4-2
- Improve the project according to the requirements of compliance improvement.

* Tue Feb 22 2022 tanyulong <tanyulong@kylinos.cn> - 3.2.4-1
- update version 3.2.4

* Tue Oct 26 2021 douyan <douyan@kylinos.cn> - 3.0.4-2
- add patch:0001-adjust-desktop-readonly-icon-agree-with-icon-view.patch

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 3.0.4-1
- update to upstream version 3.0.4

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 2.1.2-1
- Init package for openEuler
